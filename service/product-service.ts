import { ProductRepo } from "../repos/product-repository.ts";
import { Product } from "../data/types.ts";

const repo = new ProductRepo();

/**
 * 
 */
const getAll = async () => {
  const result = await repo.getAll();
  const products: Product[] = [];

  result.rows.map((p) => {
    let product: any = {};

    result.rowDescription.columns.map((elem, index) => {
      product[elem.name] = p[index];
    });

    products.push(product);
  });

  return products;
};

/**
 * 
 * @param id 
 */
const getById = async (id: number) => {
  const result = await repo.getById(id);
  const productData = result.rows.flat();
  const product: any = {};

  result.rowDescription.columns.map((elem, index) => {
    product[elem.name] = productData[index];
  });

  return product;
};

/**
 * 
 * @param name 
 */
const getByName = async (name: string) => {
  const result = await repo.getByName(name);
  const productData = result.rows.flat();
  const product: any = {};

  result.rowDescription.columns.map((elem, index) => {
    product[elem.name] = productData[index];
  });

  return product;
};

/**
 * 
 * @param product 
 */
const add = async (product: Product) => {
  let exists = await getByName(product.name);

  if (exists.id !== undefined) {
    throw new Error(`product already exists at id ${exists.id} in database`);
  }

  repo.insert(product);
};

/**
 * 
 * @param id 
 * @param data 
 */
const update = async (id: number, data: any) => {
  let product = await getById(id);

  if (!product.id) {
    throw new Error(`product with id ${id} does not exist`);
  }

  product = { ...product, ...data };
  repo.update(id, product);

  return product;
};

const remove = async (id: number) => {
  let product = await getById(id);

  if (!product.id) {
    throw new Error(`product with id ${id} does not exist`);
  }

  await repo.delete(id);

  return product;
};

export {
  getAll,
  getById,
  update,
  remove,
  add,
};
