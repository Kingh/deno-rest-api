import { Product } from "../data/types.ts";
import {
  getAll,
  getById,
  update,
  remove,
  add,
} from "../service/product-service.ts";

/**
 * @description    Get all products
 * @route    GET /api/v1/products
 */
const getProducts = async ({ response }: { response: any }) => {
  let productsDB: Product[] = await getAll();

  response.status = 200;
  response.body = {
    success: true,
    data: productsDB,
  };
};

/**
 * @description    Get a single product
 * @route    GET /api/v1/products/:id
 */
const getProduct = async (
  { params, response }: { params: { id: string }; response: any },
) => {
  const product: Product | undefined = await getById(+params.id);

  if (product) {
    response.status = 200;
    response.body = {
      success: true,
      data: product,
    };
  } else {
    response.status = 400;
    response.body = {
      success: false,
      message: `Product with id: ${params.id} does not exist`,
    };
  }
};

/**
 * @description   Add a product
 * @route    POST /api/v1/products 
 */
const addProduct = async (
  { request, response }: { request: any; response: any },
) => {
  const body = await request.body();

  if (!request.hasBody) {
    response.status = 204;
    response.body = {
      success: false,
      message: "No data in request",
    };
  } else {
    const product: Product = body.value;

    try {
      await add(product);

      console.log(response);
      response.status = 200;
      response.body = {
        success: true,
        data: product,
      };
    } catch (error) {
      response.status = 409;
      response.body = {
        success: false,
        message: error.message,
      };
    }
  }
};

/**
 * @description Update a product
 * @route PUT /api/v1/products/:id
 */
const updateProduct = async (
  { params, request, response }: {
    params: { id: string };
    request: any;
    response: any;
  },
) => {
  try {
    const body = await request.body();

    if (!request.hasBody) {
      response.status = 204;
      response.body = {
        success: false,
        message: "No data in request ",
      };
    } else {
      let product = await update(+params.id, body.value);

      response.status = 200;
      response.body = {
        success: true,
        data: product,
      };
    }
  } catch (error) {
    response.status = 400;
    response.body = {
      success: false,
      message: error.message,
    };
  }
};

/**
 * @description    Get a single product
 * @route    GET /api/v1/products/:id
 */
const deleteProduct = async (
  { params, response }: { params: { id: string }; response: any },
) => {
  try {
    await remove(+params.id);

    response.status = 200;
    response.body = {
      success: true,
      message: `Product at id ${params.id} removed`,
    };
  } catch (error) {
    response.status = 400;
    response.body = {
      success: false,
      message: error.message,
    };
  }
};

export {
  getProducts,
  getProduct,
  addProduct,
  updateProduct,
  deleteProduct,
};
