import { DBConnect } from "../db/db-connect.ts";
import { Product } from "../data/types.ts";
import { Client } from "https://deno.land/x/postgres@v0.3.11/mod.ts";

export class ProductRepo {
  private conn: Client | null = null;

  private async query(sql: string, params: any[] = []) {
    if (!this.conn) {
      this.conn = await new DBConnect().getConnection();
    }

    return this.conn.query({ text: sql, args: params });
  }

  async insert(product: Product) {
    const params: any[] = Object.values(product);
    const sql: string =
      "INSERT INTO products (name, description, price) VALUES ($1, $2, $3)";

    await this.query(sql, params);
  }

  async getAll() {
    const sql: string = "SELECT * FROM products ORDER BY id";

    return this.query(sql);
  }

  async getById(id: number) {
    const sql: string = "SELECT * FROM products WHERE id = $1";

    return this.query(sql, [id]);
  }

  async getByName(name: string) {
    const sql: string = "SELECT * FROM products WHERE name = $1";

    return this.query(sql, [name]);
  }

  async update(id: number, product: Product) {
    const params: any[] = [...Object.values(product).slice(1), id];
    const sql: string =
      "UPDATE products SET name = $1, description = $2, price = $3 WHERE id = $4";

    await this.query(sql, params);
  }

  async delete(id: number) {
    const sql: string = "DELETE FROM products WHERE id = $1";

    await this.query(sql, [id]);
  }
}
