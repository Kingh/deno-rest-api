import { Client } from "https://deno.land/x/postgres@v0.3.11/mod.ts";
import { config } from "https://deno.land/x/dotenv/mod.ts";

export class DBConnect {
  private connection: Client | null = null;

  async getConnection(): Promise<Client> {
    if (!this.connection) {
      this.connection = new Client(config());
    }

    await this.connection.connect();

    return this.connection;
  }

  async shutdown() {
    if (this.connection) {
      await this.connection.end();
    }
  }
}
